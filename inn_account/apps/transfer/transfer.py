import logging
from typing import Union
from decimal import Decimal

from django.db import models, IntegrityError, transaction

import transfer.errors as errors
from core.models import User


logger = logging.getLogger(__name__)


class Transfer:

    def __init__(self, from_user: User, amount: Decimal, to_inn_list: list = ()) -> None:
        self.from_user = from_user
        self.amount = amount
        self.destination_inn_list = list(set(to_inn_list))
        self.destination_users: User.objects = None

    def _check_amount(self):
        logger.debug('Check amount')
        if self.amount <= 0:
            raise errors.AmountError

    def _check_balance(self) -> None:
        logger.debug('Check self balance')
        if self.from_user.balance < self.amount:
            raise errors.InsufficientFundsError

    def _check_self_transfer(self) -> None:
        logger.debug('Check self transfer')
        if self.from_user.inn in self.destination_inn_list:
            raise errors.SelfTransferError

    def _check_destination_users(self) -> None:
        logger.debug('Check destination users')
        if len(self.destination_users) < len(self.destination_inn_list) or len(self.destination_users) < 1:
            raise errors.UnknownInnError

    def _get_destination_users(self) -> None:
        logger.debug('Get destination users')
        self.destination_users = User.objects.filter(inn__in=self.destination_inn_list)
        self._check_destination_users()

    def _update_destination_users_balance(self) -> None:
        amount_per_user = self.amount / len(self.destination_users)
        logger.debug(f'Increased destination users balance on {amount_per_user}')
        self.destination_users.update(balance=models.F('balance') + amount_per_user)

    def _update_self_balance(self) -> None:
        logger.debug(f'Decreased self balance on {self.amount}')
        self.from_user.balance -= self.amount
        self.from_user.save()

    @transaction.atomic
    def transfer_funds(self) -> Union[bool, None]:
        self._check_amount()
        self._check_balance()
        self._check_self_transfer()

        try:
            with transaction.atomic():
                self._get_destination_users()
                self._update_destination_users_balance()
                self._update_self_balance()
                logger.debug('Transaction commited')
        except IntegrityError:
            logger.error('Transaction rollback')
            raise errors.TransferError

        return True
