import logging

from django.shortcuts import render
from django.views.generic import View

from .errors import TransferError
from .forms import TransferForm
from .transfer import Transfer

logger = logging.getLogger(__name__)


class TransferView(View):
    template_name = "transfer.html"
    form_class = TransferForm

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form, 'result': {}})

    def post(self, request, *args, **kwargs):

        result = {'error': None, 'success': False}
        form = self.form_class(request.POST)

        if form.is_valid():
            logger.debug(f'Transfer received: {form.cleaned_data}')
            tr = Transfer(**form.cleaned_data)
            try:
                result['success'] = tr.transfer_funds()
            except TransferError as e:
                result['error'] = e.message

        return render(request, self.template_name, {'form': form, 'result': result})
