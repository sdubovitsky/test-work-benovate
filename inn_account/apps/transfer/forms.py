from django import forms

from core.models import User


class TransferForm(forms.Form):
    from_user = forms.ModelChoiceField(queryset=User.objects.all(), label='Плательщик')
    amount = forms.DecimalField(max_digits=9, decimal_places=2, label='Сумма', min_value=0)
    to_inn_list = forms.CharField(label='Инн получателей')

    def clean_to_inn_list(self):
        return [inn.strip() for inn in self.cleaned_data['to_inn_list'].split(',')]
