class TransferError(Exception):
    message = 'Ошибка перевода'


class AmountError(TransferError):
    message = 'Неверная сумма'


class InsufficientFundsError(TransferError):
    message = 'Недостаточно средств'


class SelfTransferError(TransferError):
    message = 'Невозможно пополнить собственный счет'


class UnknownInnError(TransferError):
    message = 'ИНН не найдены в системе'
