from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User


class AccountUserAdmin(UserAdmin):
    model = User
    list_display = ('username', 'inn', 'balance')
    list_filter = list_display

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'inn',  'balance', 'password1', 'password2',)
        }),
    )

    fieldsets = ((None, {'fields': ('inn', 'balance')}),) + UserAdmin.fieldsets


admin.site.register(User, AccountUserAdmin)
