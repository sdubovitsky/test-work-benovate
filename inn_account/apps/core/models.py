from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):

    inn = models.CharField(max_length=12, verbose_name='ИНН')
    balance = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='Баланс', default=0)

    def __str__(self):
        return self.username
