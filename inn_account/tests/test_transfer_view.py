from django.urls import reverse
from django.test import TestCase
from core.models import User


class TestCreateUserInGroup(TestCase):

    fixtures = ['test_users']

    def test_get(self):
        url = reverse('transfer')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'transfer.html')
        self.assertTrue('Превод отправлен' not in resp.content.decode())
        self.assertTrue('Ошибка' not in resp.content.decode())

    def test_post_positive(self):
        url = reverse('transfer')
        inn_2 = User.objects.get(username="user2").inn
        inn_3 = User.objects.get(username="user3").inn
        resp = self.client.post(url, {
            'from_user': User.objects.get(username='user1').pk,
            'amount': 100,
            'to_inn_list': f'{inn_2},{inn_3}'
        })
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'transfer.html')
        self.assertTrue('Превод отправлен' in resp.content.decode())
        self.assertTrue('Ошибка' not in resp.content.decode())

    def test_post_with_error(self):
        url = reverse('transfer')
        resp = self.client.post(url, {
            'from_user': User.objects.get(username='user1').pk,
            'amount': 100,
            'to_inn_list': f'{User.objects.get(username="user1").inn}'
        })
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'transfer.html')
        self.assertTrue('Ошибка' in resp.content.decode())
        self.assertTrue('Превод отправлен' not in resp.content.decode())

