from decimal import Decimal

from django.test import TestCase
from django.db import IntegrityError

from unittest.mock import patch

import transfer.errors as errors
from core.models import User
from transfer.transfer import Transfer


class TransferHandlerTest(TestCase):

    fixtures = ['test_users']

    def test_amount_error(self):
        from_user = User.objects.get(username='user1')
        tr = Transfer(from_user, -from_user.balance, ['user2', 'user3'])
        self.assertRaises(errors.AmountError, tr.transfer_funds)

    def test_balance_error(self):
        from_user = User.objects.get(username='user1')
        tr = Transfer(from_user, from_user.balance + 1, ['user2', 'user3'])
        self.assertRaises(errors.InsufficientFundsError, tr.transfer_funds)

    def test_self_transfer_error(self):
        from_user = User.objects.get(username='user1')
        tr = Transfer(from_user, from_user.balance, ['user1', 'user2'])
        self.assertRaises(errors.SelfTransferError, tr.transfer_funds)

    def test_transfer_unknown_inn_error(self):
        from_user = User.objects.get(username='user1')
        tr = Transfer(from_user, from_user.balance, ['user2', 'user3', 'user100'])
        self.assertRaises(errors.UnknownInnError, tr.transfer_funds)

    def test_empty_inn_list(self):
        from_user = User.objects.get(username='user2')
        tr = Transfer(from_user, from_user.balance, [])
        self.assertRaises(errors.UnknownInnError, tr.transfer_funds)

    def test_positive_transaction(self):
        from_user = User.objects.get(username='user1')
        tr = Transfer(from_user, from_user.balance, ['user2', 'user3'])
        result = tr.transfer_funds()
        self.assertEqual(result, True)

        from_user = User.objects.get(username='user1')
        user_2 = User.objects.get(username='user2')
        user_3 = User.objects.get(username='user3')

        self.assertEqual(from_user.balance, 0)
        self.assertEqual(user_2.balance, 150)
        self.assertEqual(user_3.balance, 150)

    def test_transaction_users_with_same_inn(self):
        from_user = User.objects.get(username='user2')

        tr = Transfer(from_user, from_user.balance, ['user1', 'user3'])
        tr.transfer_funds()

        from_user = User.objects.get(username='user2')
        user_1 = User.objects.get(username='user1')
        user_3 = User.objects.get(username='user3')
        user_4 = User.objects.get(username='user4')

        self.assertEqual(from_user.balance, 0)
        self.assertEqual(user_1.balance, Decimal('133.33'))
        self.assertEqual(user_3.balance, Decimal('133.33'))
        self.assertEqual(user_4.balance, Decimal('133.33'))

    @patch('transfer.transfer.Transfer._update_self_balance')
    def test_transaction_rollback(self, update_balance_mock):
        update_balance_mock.side_effect = IntegrityError

        from_user = User.objects.get(username='user1')
        tr = Transfer(from_user, from_user.balance, ['user2', 'user3'])
        self.assertRaises(errors.TransferError, tr.transfer_funds)

        user_1 = User.objects.get(username='user1')
        user_2 = User.objects.get(username='user2')
        user_3 = User.objects.get(username='user3')

        self.assertEqual(user_1.balance, 100)
        self.assertEqual(user_2.balance, 100)
        self.assertEqual(user_3.balance, 100)
