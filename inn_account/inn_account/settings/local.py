import sys

from .base import *

DEBUG = True

ALLOWED_HOSTS = [
    '*'
]

FIXTURE_DIRS = [
    os.path.join(BASE_DIR, 'fixtures')
]

AUTH_PASSWORD_VALIDATORS = []
